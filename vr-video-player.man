.TH vr-video-player 1 "JUNE 2020"
.SH NAME
vr-video-player \- A virtual reality video player for x11 on Linux
.SH SYNOPSIS
.B vr-video-player
[\fB\-\-flat\fR]
[\fB\-\-left\-right\fR|\fB\-\-right\-left\fR]
[\fB\-\-stretch\fR|\fB\-\-no\-stretch\fR]
[\fB\-\-zoom \fIzoom\-level\fR]
<\fIwindow_id\fR>
.SH DESCRIPTION
.B vr-video-player
is an early in development virtual reality video player for Linux running X11, based on Valve's openvr hellovr_opengl sample code.
.SS "Play Video"
Start a video in your video player of choice (tested with mpv) and then get the x11 window id (this can be done with xwininfo) and then fullscreen the video (for best quality).
Then launch vr-video-player with the x11 window id.
If the video is not meant to be viewed as a sphere but as a rectangle, pass the --flat option.
If the right eye is shown on the left side, pass the --right-left option.
If the video is stretched, pass the --no-stretch option. Note: This option only works when also using the --flat option.
.SS "Play Non-VR Games"
vr-video-player can also be used to play games in VR to to get a 3D effect, and even for games that don't support VR.
For games that have built-in side-by-side view, you can launch the game with side-by-side view enabled and get the X11 window id of the game.
Then launch vr-video-player with the --flat option.
For games that do not have built-in side-by-side view, you can use ReShade and SuperDepth3D_VR.fx effect with proton. This will make the game render with side-by-side view and you can then get the X11 window id of the game and launch vr video player with the --flat option. The game you are playing might require settings to be changed manually in ReShade for SuperDepth3D_VR to make it look better.
.SH OPTIONS
.IP --flat
For non spherical video
.IP --left-right
Flipps left and right video
.IP --no-stretch
Disables video stretching. Note: This option only works when also using the 
.BR --flat
option.
.IP "--zoom \fIzoom\-level\fR"
Floating point value to zoom in or out
.SS "Keyboard Control"
.IP "w OR vr-controller trigger"
Re-centers the viewport
.SH AUTHOR
DEC05EBA <dec05eba@protonmail.com>
.SH "SEE ALSO"
.BR xwininfo (1),
.BR mpv (1)
